const trainer = {
  name: 'Ash',
  age: 10,
  pokemon: ['Pikachu', 'Charmander', 'Squirtle'],
  friends: {
    Hoenn: ['May', 'Max'],
    Kanto: ['Brock', 'Misty'],
  },
  talk: function (pokemon) {
    console.log(`${pokemon} I choose you!`)
  },
}

console.log(trainer.name)

console.log(trainer['age'])

trainer.talk('Pikachu')

console.log(trainer)

function Pokemon(name, level) {
  this.name = name
  this.level = level
  this.health = level * 10
  this.attack = level * 5
}

const pikachu = new Pokemon('Pikachu', 7)
console.log(pikachu)

const geodude = new Pokemon('Geodude', 5)
console.log(geodude)

const mewtwo = new Pokemon('Mewtwo', 100)
console.log(mewtwo)

let attacker = pikachu
let defender = geodude

while (attacker.health > 0 && defender.health > 0) {
  // Attacker uses attack on defender
  console.log(`${attacker.name} attacks ${defender.name}!`)
  defender.health -= attacker.attack
  console.log(`${defender.name}'s health is now ${defender.health}.`)

  // Swap attacker and defender
  ;[attacker, defender] = [defender, attacker]
}

// Check which Pokemon fainted
if (pikachu.health <= 0) {
  console.log(`${pikachu.name} fainted!`)
} else {
  console.log(`${geodude.name} fainted!`)
}
